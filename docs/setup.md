# MkDocs / Resources #

## MkDocs ##

*   [MkDocs website](http://www.mkdocs.org/)
    + [Writing your docs](http://www.mkdocs.org/user-guide/writing-your-docs/) - explains file organization, formatting, document links
*   Cài đặt:
```Bash
pip3 install mkdocs
```

## Material Theme ##

*   [Material](http://squidfunk.github.io/mkdocs-material/)
    + [Getting Started](http://squidfunk.github.io/mkdocs-material/getting-started/) - also contains useful MkDocs general information
    + [Material Design](https://material.io) - user interface components that go beyond MkDocs
    + [Material icons](https://material.io/icons/) - modern and clean icons
* Cài đặt:
```Bash
pip3 install mkdocs-material #version 4.6.3
```
*   Nâng cấp:
```Bash
 pip3 install --upgrade mkdocs-material
```
*   Cấu hình mkdocs.yml:
```
theme:
  name: 'material'
```

## Code Tabs
*   [Markdown-fencedcode-tabs](https://github.com/yacir/markdown-fenced-code-tabs) - is an extension that generates a HTML structure for consecutive fenced code blocks content
*   Cài đặt:
```Bash
pip3 install markdown-fenced-code-tabs
```
*   Cấu hình mkdocs.yml:
```
markdown_extensions:
  - markdown_fenced_code_tabs:
      single_block_as_tab: False
      active_class: 'active'
      template: 'default'
```

## Code Highlight
*   [Markdown-codehilite](https://squidfunk.github.io/mkdocs-material/extensions/codehilite/) - is an extension that adds syntax highlighting to code blocks and is included in the standard Markdown library. The highlighting process is executed during compilation of the Markdown file
*   Cài đặt:
```Bash
pip3 install pygments
```
*   Cấu hình mkdocs.yml:
```
markdown_extensions:
  - codehilite
```

## Footnotes
*   [Markdown-footnotes](https://squidfunk.github.io/mkdocs-material/extensions/footnotes/) - is another extension included in the standard Markdown library. As the name says, it adds the ability to add footnotes to your documentation
*   Cấu hình mkdocs.yml:
```
markdown_extensions:
  - footnotes
```

## PyMDown
*   [Markdown-footnotes](https://squidfunk.github.io/mkdocs-material/extensions/footnotes/) - is another extension included in the standard Markdown library. As the name says, it adds the ability to add footnotes to your documentation
*   Cài đặt:
```Bash
pip3 install pymdown-extensions
```
*   Cấu hình mkdocs.yml:
```
markdown_extensions:
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.superfences
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
```
## Mermaid 
*   [Mermaid](https://mermaidjs.github.io/) - generation of diagrams and flowcharts from text in a similar manner as markdown.
*   Cài đặt:
```Bash
pip install mkdocs-mermaid-plugin
```
*   Cấu hình mkdocs.yml:
```
markdown_extensions:
 - pymdownx.superfences:
   custom_fences:
     - name: mermaid
       class: mermaid
       format: !!python/name:pymdownx.superfences.fence_div_format

plugins:
  - markdownmermaid

extra_javascript:
  - https://unpkg.com/mermaid@7.1.2/dist/mermaid.min.js
  - https://unpkg.com/mermaid@7.1.2/dist/js/mermaid.min.js.map       
```

## Pdf Export

*   [Pdf Export](https://github.com/zhaoterryy/mkdocs-pdf-export-plugin) - An MkDocs plugin to export content pages as PDF files

*   Cài đặt:
```Bash
pip3 install mkdocs-pdf-export-plugin
```

*   Cấu hình mkdocs.yml:
```
plugins:
  - pdf-export:
    verbose: true
    media_type: print
    enabled_if_env: ENABLE_PDF_EXPORT
```

## Drawio Exporter

*   [Drawio Exporter](https://pypi.org/project/mkdocs-drawio-exporter/) - Exports your Draw.io diagrams at build time for easier embedding into your documentation.

*   Cài đặt:
```Bash
pip3 install mkdocs-drawio-exporter
```

*   Cấu hình mkdocs.yml:
```
plugins:
  - drawio-exporter:
      # Diagrams are cached to speed up site generation. The default path is
      # drawio-exporter, relative to the documentation directory.
      cache_dir: 'drawio-exporter'
      # Path to draw.io or draw.io.exe. Will be determined from the PATH
      # environment variable if not specified.
      drawio_executable: /Applications/draw.io.app/Contents/MacOS/draw.io
      # Output format (see draw.io --help | grep format)
      format: jpg
      # Glob pattern for matching source files
      sources: '*.drawio'
```

## Drawio Offline
*   [Drawio Offline](https://github.com/jgraph/drawio-desktop/releases/tag/v12.5.3) - draw.io allows you to work on your diagrams while you are offline. You can use draw.io with almost all of the popular browsers: Chrome, Firefox and MS Edge. If you are using Safari, you will need to load the offline app while you are connected to the internet.

## Git revision date localized plugin
*   [Git] - mkdocs-git-revision-date-localized-plugin is an extension that shows the date on which a Markdown file was last updated in Git at the bottom of each page. The date is extracted at the time of the build, so mkdocs build must be triggered from within a Git repository.

*   Cài đặt:
```Bash
pip3 install mkdocs-git-revision-date-localized-plugin
```

*   Cấu hình mkdocs.yml:
```
plugins:
  - git-revision-date-localized
```

## Markdown ##

* [Markdown on Wikipedia](https://en.wikipedia.org/wiki/Markdown)
* [Mastering Markdown on GitHub](https://guides.github.com/features/mastering-markdown/)
* [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [GitHub Markdown language support](https://github.com/github/linguist/blob/master/lib/linguist/languages.yml) - use to format code blocks
